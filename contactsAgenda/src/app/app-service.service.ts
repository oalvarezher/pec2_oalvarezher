import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppServiceService {

  contactList: Contact[];

  id: number;

  constructor() {
    this.contactList = this.getLocalStorage();
    /* Sino tenemos la clave en el localstorage, se encarga de crearla con el contacto por defecto. 
    Además, pone el id a 1 ya que el 0 lo tiene el contacto por defecto */
    if (!this.contactList) {
      this.contactList = [{ id: 0, firstName: 'John', lastName: 'Doe',
      phoneNumber: 123456789, email: 'john@gmail.com', adress: 'C/ adress' }];
      this.id = 1;
      this.setLocalStorage();
    /* Si ya tenemos la clave creada pero el listado vacío, lo único que hacemos es obtener el listado vacío
    y poner el id a 0 para poder introducir el primer contacto */
    } else if (this.contactList.length === 0) {
      this.contactList = this.getLocalStorage();
      this.id = 0;
    /* Si ya tenemos la clave creada y el lista lleno, obtenemos dicho listado y nuestro id tendrá el id + 1 al último 
    id que tengamos */
    } else {
      this.contactList = this.getLocalStorage();
      this.id = this.contactList[this.contactList.length - 1].id + 1;
    }
  }

  /* Elimina un contacto */
  removeContact(id: number) {
    this.contactList = this.contactList.filter(element => element.id !== id);
    this.setLocalStorage();
    this.contactList = this.getLocalStorage();
  }

  /* Añadimos un contacto */
  add(contact: Contact) {
    contact.id = this.id;
    this.id++;
    this.contactList.push(contact);
    this.setLocalStorage();
    this.contactList = this.getLocalStorage();
  }

  /* Editamos un contacto */
  edit(contactEdit: Contact) {
    for (let contact of this.contactList) {
      console.log(contact.id);
      console.log(contactEdit.id);
      if (contact.id === contactEdit.id) {
        contact = contactEdit;
      }
    }
    this.setLocalStorage();
    this.contactList = this.getLocalStorage();
  }

  /* Obtenemos un contacto mediante un id */
  getContact(id: string): Observable<Contact> {
    return of(this.contactList.find(element => element.id === parseInt(id)));
  }

  /* Obtenemos el listado del localstorage */
  getLocalStorage() {
    return JSON.parse(localStorage.getItem('contactList'));
  }

  /* Guardamos la lista en el localstorage */
  setLocalStorage() {
    localStorage.setItem('contactList', JSON.stringify(this.contactList));
  }

}

/* Clase contacto */
export class Contact {
  id: number;
  firstName: string;
  lastName: string;
  phoneNumber: number;
  email: string;
  adress: string;
}
