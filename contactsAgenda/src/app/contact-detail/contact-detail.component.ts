import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Contact, AppServiceService } from '../app-service.service';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.scss'],
  providers: [MessageService]
})
export class ContactDetailComponent implements OnInit {

  /* Formulario para editar el contacto */
  contactForm: FormGroup;

  /* Contacto que se va a editar */
  contact: Contact;

  constructor(
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private appService: AppServiceService,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.getContact(params['id']);
    });
  }

  /* Obtiene el contacto a través del id llamando al servicio */
  getContact(id: string) {
    this.appService.getContact(id).subscribe(
      contact => {
        this.contact = contact;
        this.buildForm();
      }
    );
  }

  /* Construye el formulario con el contacto que hemos obtenido */
  buildForm() {
    this.contactForm = new FormGroup({
      firstName: new FormControl(this.contact.firstName, [Validators.required]),
      lastName: new FormControl(this.contact.lastName, [Validators.required]),
      phone: new FormControl(this.contact.phoneNumber, [Validators.minLength(9)]),
      email: new FormControl(this.contact.email, [Validators.email]),
      adress: new FormControl(this.contact.adress)
    });
  }

  /* Permite volver a la pantalla de inicio */
  back() {
    this.route.navigate(['/home']);
  }

  /* Guarda los cambios que hemos realizado sobre el contacto */
  saveChanges() {
    this.contact.firstName = this.contactForm.controls.firstName.value;
    this.contact.lastName = this.contactForm.controls.lastName.value;
    this.contact.phoneNumber = this.contactForm.controls.phone.value;
    this.contact.email = this.contactForm.controls.email.value;
    this.contact.adress = this.contactForm.controls.adress.value;
    this.appService.edit(this.contact);
    this.back();
  }

  /* Cancela los cambios y vuelve a inicio */
  cancelChanges() {
    this.contactForm.reset();
    this.back();
  }

  /* Muestra el popup para poder confirmar el proceso de borrado */
  showConfirm() {
    this.messageService.add({
      sticky: true, severity: 'warn', summary: '¿Estás seguro de borrar el contacto?',
      detail: 'Confirma para proceder'
    });
  }

  /* Limpia todos los mensajes */
  clearMessages() {
    this.messageService.clear();
  }

  /* Borra el contacto */
  deleteContact() {
    this.appService.removeContact(this.contact.id);
    this.back();
  }

}
