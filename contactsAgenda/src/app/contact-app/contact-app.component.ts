import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-app',
  templateUrl: './contact-app.component.html',
  styleUrls: ['./contact-app.component.scss']
})
export class ContactAppComponent implements OnInit {

  author = 'Omar Adolfo Álvarez Hernández';
  isDetailComponent: boolean;

  constructor(
    private route: Router,
  ) { }

  ngOnInit() {
    this.route.url === '/home' ? this.isDetailComponent = false : this.isDetailComponent = true;
  }

  back() {
    this.route.navigate(['/home']);
  }

}
