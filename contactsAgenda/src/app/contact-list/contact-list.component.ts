import { Component, OnInit } from '@angular/core';
import { AppServiceService, Contact } from '../app-service.service';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss'],
  providers: [MessageService]
})
export class ContactListComponent implements OnInit {

  /* Permite guardar el id durante el proceso de confirmación */
  id: number;

  constructor(
    public appService: AppServiceService, // Se usa el array del servicio */
    private messageService: MessageService,
  ) { }

  ngOnInit() {}

  /* Borra el contacto cuando el hijo emite */
  removeContact() {
    this.appService.removeContact(this.id);
    this.clearMessages();
  }

  /* Muestra el popup para poder confirmar el proceso de borrado */
  showConfirm(event: any) {
    this.id = event.id;
    this.messageService.add({
      sticky: true, severity: 'warn', summary: '¿Estás seguro de borrar el contacto?',
      detail: 'Confirma para proceder'
    });
  }

  /* Limpia todos los mensajes */
  clearMessages() {
    this.messageService.clear();
  }

}
