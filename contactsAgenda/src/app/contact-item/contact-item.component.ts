import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent implements OnInit {

  /* Recibe los parámetros del componente padre */

  @Input() id: number;

  @Input() firstNameTxt: string;

  @Input() lastNameTxt: string;

  @Output() remove = new EventEmitter();

  constructor(
    private route: Router,
  ) { }

  ngOnInit() {
  }

  /* Ir a la pantalla de detalle del contacto */
  goDetail(id: number) {
    this.route.navigate(['contactDetail/' + id]);
  }

  /* Borra el contacto emitiendo el id al componente padre */
  removeContact(id: number) {
    this.remove.emit({id});
  }

}
