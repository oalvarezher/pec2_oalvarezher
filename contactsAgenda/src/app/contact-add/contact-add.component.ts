import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Contact, AppServiceService } from '../app-service.service';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.scss'],
  providers: [MessageService]
})
export class ContactAddComponent implements OnInit {

  /* Formulario para añadir un nuevo contacto */
  contactForm = new FormGroup({
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.minLength(9)]),
    email: new FormControl('', [Validators.email]),
    adress: new FormControl('')
  });

  /* Objeto contacto */
  contact: Contact;

  constructor(
    private appService: AppServiceService,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
  }

  /* Crear el objeto con los elementos del formulario para luego llamar al servicio que se va a encargar de añadir el contacto */
  saveNewContact() {
    this.contact = new Contact();
    this.contact.id = null;
    this.contact.firstName = this.contactForm.controls.firstName.value;
    this.contact.lastName = this.contactForm.controls.lastName.value;
    this.contact.phoneNumber = this.contactForm.controls.phone.value;
    this.contact.email = this.contactForm.controls.email.value;
    this.contact.adress = this.contactForm.controls.adress.value;
    this.appService.add(this.contact);
    this.messageService.add({ severity: 'success', summary: '¡Éxito!', detail: 'Se ha añadido un nuevo contacto' });
    this.resetContact();
  }

  /* Limpia los campos del formulario */
  resetContact() {
    this.contactForm.reset();
  }

}
